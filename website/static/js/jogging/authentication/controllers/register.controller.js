/**
 * Created by abomariam on 24/01/16.
 */
(function(){
    'use strict';

    angular
        .module('jogging.authentication.controllers')
        .controller('RegisterController',RegisterController);

    RegisterController.$inject = ['$location','$scope','Authentication'];

    function RegisterController($location,$scope,Authentication){
        var vm = this;

        vm.register = register;


        activate();

        function activate(){
            if (Authentication.isAuthenticated()){
                $location.url('/');
            }
        }


        function register(){
            Authentication.register(vm.form)
                .then(successFn,errorFn);

            function successFn(data){
                Authentication.login(vm.form);

            }

            function errorFn(data){
                for (var field in data.data){

                    $('#'+field).addClass('invalid');

                    for (var error in data.data[field]){
                         Materialize.toast(field+ ': ' + data.data[field][error], 8000)

                    }
                }
            }


        }


    }
})();