/**
 * Created by abomariam on 24/01/16.
 */
(function(){
    'use strict';

    angular
        .module('jogging.authentication.controllers')
        .controller('LoginController',LoginController);

    LoginController.$inject = ['$location','$scope','Authentication'];


    function LoginController($location,$scope,Authentication){
        var vm = this;

        vm.login = login;

        activate();

        function activate(){
            if (Authentication.isAuthenticated()){
                $location.url('/');
            }
        }


        function login(){
            Authentication.login(vm.form)
                .then(successFn);

            function successFn(data){
                for (var field in data.data){
                    $('#'+field).addClass('invalid');
                    for (var error in data.data[field]){
                         Materialize.toast(field+ ': ' + data.data[field][error], 8000);
                    }
                }
            }
        }
    }
})();