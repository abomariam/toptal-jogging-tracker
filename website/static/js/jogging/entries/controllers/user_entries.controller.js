/**
 * Created by abomariam on 25/01/16.
 */
(function(){
  'use strict';


    angular
        .module('jogging.entries.controllers')
        .controller('UserEntriesController',UserEntriesController);

    UserEntriesController.$inject = ['$location','$routeParams','Authentication','Entries']

    function UserEntriesController($location,$routeParams,Authentication,Entries) {
        var vm = this;


        vm.destroy = destroy;
        vm.edit = edit;
        vm.cancel = cancel;
        vm.update = update;
        vm.save = save;

        vm.editEntry = undefined;

        activate();

        function activate() {

            var username = $routeParams.username.substr(1);

            if (!Authentication.isAuthenticated() || Authentication.getAuthenticatedAccount().username != username) {
                if (!Authentication.getAuthenticatedAccount().roles.is_admin) {
                    $location.url('/');
                    Materialize.toast('You aren\'t authorized to view these Entries', 4000);
                    return;
                }
            }

            Entries.getUserEntries(username).then(successFn, errorFn);

            function successFn(data) {
                vm.entries = data.data;
                calculateReport();
            }

            function errorFn(data) {
                for (var field in data.data) {

                    for (var error in data.data[field]) {
                        Materialize.toast(field + ': ' + data.data[field][error], 8000)

                    }
                }
            }
        }
        function calculateReport(){
            var avg_speed = 0;
            vm.distance = 0;
            for (var entry in vm.entries) {
                if (isWithinLastWeek(vm.entries[entry].date)) {
                    avg_speed += vm.entries[entry].avg_speed;
                    vm.distance += vm.entries[entry].distance;
                }
                vm.avg_speed = avg_speed / 7.0;
            }

            function isWithinLastWeek(date) {
                var now = new Date();
                var last_week = new Date();
                last_week.setDate(last_week.getDate() - 7);

                function parseDate(input) {
                    var parts = input.split('-');
                    var date = new Date(parts[0], parts[1] - 1 || '0', parts[2] || '01');
                    return date;
                }

                var temp = parseDate(date);
                if (temp >= last_week && temp <= now) {
                    return true;
                } else {
                    return false;
                }
            }
        }
       function destroy(id){
           Entries.destroyEntry(id).then(successFn,errorFn);

           function successFn(data){
               for (var i= 0; i < vm.entries.length; i++){
                if (vm.entries[i].id == id){
                    vm.entries.pop(i);
                    break;
                }
            };
           }

           function errorFn(data){
               Materialize.toast("You do not have permission to perform this action.",4000);
           }
       }

        function edit(id){
            for (var entry in vm.entries){
                if (vm.entries[entry].id == id){
                    vm.editEntry = $.extend(true, {}, vm.entries[entry]);
                    break;
                }
            }

        }
        function cancel(){
            vm.editEntry = undefined;
        }

        function update(){
            Entries.update(vm.editEntry.id,vm.editEntry).then(successFn,errorFn);

            function successFn(data){
                for (var entry in vm.entries){
                if (vm.entries[entry].id == vm.editEntry.id){
                    vm.entries[entry] = data.data;
                    vm.editEntry = undefined;
                    calculateReport();
                    break;
                }
            }
            }
            function errorFn(data){

               for (var field in data.data){

                    $('#'+field+'_edit').addClass('invalid');

                    for (var error in data.data[field]){
                         Materialize.toast(field+ ': ' + data.data[field][error], 8000)

                    }
                }
            }

        }


        function save(){
            var username = $routeParams.username.substr(1);
            Entries.save(username,vm.tempEntry).then(successFn,errorFn);

            function successFn(data){
                vm.entries.push(data.data);
                calculateReport();
                vm.tempEntry = undefined;
                $('#modal1').closeModal();
            }
            function errorFn(data){
               for (var field in data.data){

                    $('#'+field).addClass('invalid');

                    for (var error in data.data[field]){
                         Materialize.toast(field+ ': ' + data.data[field][error], 8000)

                    }
               }
            }
        }




    }
})();