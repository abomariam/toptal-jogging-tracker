/**
 * Created by abomariam on 25/01/16.
 */
(function(){
    'use strict';

    angular
        .module('jogging.entries',[
            'jogging.entries.controllers',
            'jogging.entries.services',
            'jogging.entries.filters'
        ]);

    angular
        .module('jogging.entries.controllers',[]);

    angular
        .module('jogging.entries.services',[]);

    angular
        .module('jogging.entries.filters',[]);


    angular
        .module('jogging.entries.filters')
        .filter("dateFilter", dateFilter);


    function dateFilter() {

          return function(items, from, to) {

              if (!items){
                  return items;
              }
              if (!from || from.length < 4){
                  from='1970-01-01';
              }
              if (!to || to.length < 4){
                  to = '3000-01-01';
              }

              function parseDate(input) {
                  var parts = input.split('-');
                  var date = new Date(parts[0],parts[1]-1 || '0' ,parts[2]||'01');
                  return date;
                }

                var df = parseDate(from);

                var dt = parseDate(to);
                var result = [];
                for (var i=0; i<items.length; i++){
                    if (items[i].date.length != 10){
                        result.push(items[i]);
                    }
                    var tf = parseDate(items[i].date);
                    if (tf >= df && tf <= dt)  {
                        result.push(items[i]);
                    }
                }
                return result;
          };
    }

})();