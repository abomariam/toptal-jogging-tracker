/**
 * Created by abomariam on 25/01/16.
 */
(function(){
    'use strict';

    angular
        .module('jogging.entries.services')
        .factory('Entries',Entries);

    Entries.$inject = ['$http','Authentication'];

    function Entries($http,Authentication){
        var Entries = {
            getUserEntries:getUserEntries,
            destroyEntry:destroyEntry,
            update:update,
            save:save
        };

        return Entries;

        function getUserEntries(username){
            return $http.get('/api/v1/accounts/+'+username+'/entries/')
        }

        function destroyEntry(id){
            return $http.delete('/api/v1/entries/'+id+'/')
        }

        function update(id,data){
            return $http.put('/api/v1/entries/'+id+'/',data)
        }
        function save(username,data){
            return $http.post('/api/v1/accounts/+'+username+'/entries/',data);
        }
    }
})();