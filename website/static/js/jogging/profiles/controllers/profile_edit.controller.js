/**
 * Created by abomariam on 25/01/16.
 */
(function(){
    'use strict';

    angular
        .module('jogging.profiles.controllers')
        .controller('ProfileEditController',ProfileEditController);

    ProfileEditController.$inject = ['$location','$routeParams','Profile','Authentication'];

    function ProfileEditController($location,$routeParams, Profile, Authentication){

        var vm = this;

        vm.update = update;
        vm.destroy = destroy;
        vm.user = undefined;

        vm.authenticatedUser = Authentication.getAuthenticatedAccount();

        activate();

        function activate(){
            var username = $routeParams.username.substr(1);

            if (!Authentication.isAuthenticated() || Authentication.getAuthenticatedAccount().username != username){
                if (!(Authentication.getAuthenticatedAccount().roles.is_admin || Authentication.getAuthenticatedAccount().roles.is_manager)) {
                    $location.url('/');
                    Materialize.toast('You aren\'t authorized to view this profile', 8000);
                    return;
                }
            }

            Profile.get(username).then(profileSuccessFn, profileErrorFn);

            function profileSuccessFn(data) {
              vm.user = data.data;
            }

            function profileErrorFn(data) {
                $location.url('/');
                Materialize.toast('That user does not exist.', 8000);
            }
        }

        function update(){

            var username = $routeParams.username.substr(1);

            Profile.update(username,vm.user).then(updateSuccessFn,updateErrorFn);

            function updateSuccessFn(data){

                Materialize.toast('Your Profile updated successfully',4000);
                $location.url('/+'+vm.user.username+'/profile');
            }

             function updateErrorFn(data){
                for (var field in data.data){
                    $('#'+field).addClass('invalid');
                    for (var error in data.data[field]){
                         Materialize.toast(field+ ': ' + data.data[field][error], 8000)
                    }
                }
            }
        }


        function destroy(){

            var username = $routeParams.username.substr(1);

            Profile.destroy(username).then(successFn, errorFn);

            function successFn(data){
                if (Authentication.getAuthenticatedAccount().username == username) {
                    Authentication.unauthenticate();
                    window.location = '/';
                } else {
                    $location.url('/dashboard');
                    Materialize.toast('Account deleted successfully.', 4000);
                }

            }

            function errorFn(data){
                for (var field in data.data){
                    for (var error in data.data[field]){
                         Materialize.toast(field+ ': ' + data.data[field][error], 8000)
                    }
                }
            }

        }
    }
})();