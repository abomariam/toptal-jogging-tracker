/**
 * Created by abomariam on 24/01/16.
 */
(function () {
    'use strict';

    angular
        .module('jogging.config')
        .config(config)

    config.$inject = ['$locationProvider'];

    function config($locationProvider){
        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('!');
    }
}

)();