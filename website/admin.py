from django.contrib import admin
from models import Entry,Roles
from django.contrib.auth.models import Group, User
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
# Register your models here.



class RolesInLine(admin.StackedInline):
    model = Roles
    can_delete = False

class UserRolesAdmin(UserAdmin):
    inlines = (RolesInLine,)
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',)}),
    )



admin.site.unregister(Group)
admin.site.unregister(User)

admin.site.register(User,UserRolesAdmin)
admin.site.register(Entry)