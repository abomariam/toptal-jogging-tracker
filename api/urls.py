
from django.conf.urls import url,include
from api.views import RegisterView,LoginView,LogoutView,UpdateProfileView,AdminUsersView,EntryView


urlpatterns = [
    # url(r'^', include(router.urls)),
    url(r'^accounts/register/$', RegisterView.as_view()),
    url(r'^accounts/\+(?P<username>[^/]*)/$', UpdateProfileView.as_view()),

    url(r'^admin/accounts/$', AdminUsersView.as_view({'get': 'list','post':'create'})),
    url(r'^admin/accounts/\+(?P<username>.*)/$', AdminUsersView.as_view({'get': 'retrieve','put':'update','delete':'destroy'})),

    url(r'^auth/login/', LoginView.as_view()),
    url(r'^auth/logout/', LogoutView.as_view()),

    url(r'^accounts/\+(?P<username>.*)/entries/$',EntryView.as_view({'get':'list','post':'create'})),
    url(r'^entries/(?P<pk>.*)/$',EntryView.as_view({'delete':'destroy','put':'update'})),
]
